#!/bin/bash

NAME="190.113.247.219" # Name of the application
DJANGODIR=/home/devel/sites/sentinel-mikrotik/sentinel-mikrotik # Django project directory
LOGFILE=/var/log/gunicorn/gunicorn_mikrotik.log
LOGDIR=$(dirname $LOGFILE)
SOCKFILE=/home/devel/sites/sentinel-mikrotik/run/gunicorn.sock # we will communicate using this unix socket
LOGDIR_PROJECT=/home/devel/sites/sentinel-mikrotik/logs # we will communicate using this unix socket
USER=devel # the user to run as
GROUP=devel # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings.local # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi
TIMEOUT=600
#mikrotik si escuchara por ip
ADDRESS=0.0.0.0:8080
echo "Starting $NAME as `whoami`"


alias python='/usr/bin/python3.6'
#alias gunicorn='/home/devel/.local/bin/gunicorn'


# Activate the virtual environment
#source /home/devel/sites/environments/sentinel/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
--workers $NUM_WORKERS \
--bind=$ADDRESS \
--user=$USER --group=$GROUP \
--log-level=debug \
--log-file=$LOGFILE 2>>$LOGFILE
