from django.urls import path, re_path
from rest_framework import routers

from utp.datatables import DatatableUTP, DatatableCPEUTP, DatatablePlan, DatatableProfile, \
							DatatableRadius, DatatableSystemProfile
from dashboard.datatables import DatatableLOG


urlpatterns = [
    
    path('datatables/utp', DatatableUTP.as_view() , name="datatable_utp"),
    path('datatables/logs/', DatatableLOG.as_view() , name="datatable_logs"),
    re_path(r'^datatables/utp_logs/(?P<uuid>[-\w]+)/$', DatatableLOG.as_view() , name="datatable_utp_logs"),
    re_path(r'^datatables/utp_cpe/(?P<uuid>[-\w]+)/$', DatatableCPEUTP.as_view() , name="datatable_cpe_utp"),
    path('datatables/utp_cpe', DatatableCPEUTP.as_view() , name="datatable_cpeutp_list"),
    path('datatables/planes', DatatablePlan.as_view() , name="datatable_plan"),
    re_path(r'^datatables/profiles/(?P<uuid>[-\w]+)/$', DatatableProfile.as_view() , name="datatable_profile"),
    path('datatables/systemprofiles', DatatableSystemProfile.as_view() , name="datatable_profile"),
    path('datatables/radius', DatatableRadius.as_view() , name="datatable_radius"),
]

