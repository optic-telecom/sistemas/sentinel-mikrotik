import json
from django.views.generic.base import TemplateView
from django.shortcuts import get_list_or_404
from django.utils.html import escape
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model

from django_datatables_view.base_datatable_view import DatatableMixin
from django_datatables_view.mixins import JSONResponseView,JSONResponseMixin, LazyEncoder

from rest_framework_jwt.settings import api_settings
from rest_framework.response import Response

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
User = get_user_model()


@method_decorator(csrf_exempt, name='dispatch')
class DataTable(DatatableMixin, JSONResponseMixin, TemplateView):

    def get_initial_queryset(self):
        try:
            return self.model.objects.actives()
        except:
            return self.model.objects.filter(status_field=True)
        

    def render_column(self, row, column):
        """ Renders a column on a row. column can be given in a module notation eg. document.invoice.type
        """
        # try to find rightmost object
        obj = row
        parts = column.split('.')
        for part in parts[:-1]:
            if obj is None:
                break
            obj = getattr(obj, part)

        
        # try using display_OBJECT for choice fields
        if hasattr(self, 'display_%s' % parts[-1]):
            value = getattr(self, 'display_%s' % parts[-1])(obj)
        else:
            value = getattr(obj, parts[-1], None)

            if self.escape_values:
                value = escape(value)            
        
            if value and hasattr(obj, 'get_absolute_url'):
                return '<a href="%s">%s</a>' % (obj.get_absolute_url(), value)

            if value is None:
                value = self.none_string
            
        return value

    def get(self, request, *args, **kwargs):
            
        if 'pk' in kwargs:
            self.pk = kwargs['pk']

        if 'uuid' in kwargs:
            self.uuid = kwargs['uuid']

        return JSONResponseMixin.get(self, request=request, *args,**kwargs)
        
        # token = self.request.META.get("HTTP_AUTHORIZATION")
        # try:
        #     decode = jwt_decode_handler(token)
        #     user = User.objects.get(username=decode['username'])
        #     return JSONResponseMixin.get(self, request=request, *args,**kwargs)

        # except Exception as e:
        #     response = {'result': 'error',
        #                 'sError': str(e),
        #                 'text': str(e)}
        #     print ("in get datatable", e)
        #     dump = json.dumps(response, cls=LazyEncoder)
        #     return self.render_to_response(dump)




class MixinListRetrive(object):

    lookup_url_kwarg = 'uuid'

    def retrieve(self, request, *args, **kwargs):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )        
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        queryset = get_list_or_404(self.queryset, **filter_kwargs)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)



class ButtonDataTable(object):

    def __call__(self):
        return self.sep.join(self.list_btn)

    def __init__(self, **options):
        self.template = '<a href="{link}" type="button" class="btn btn-{class_css}" title="{title}"><i class="fa fa-{ico}"></i></a>'
        self.template_btn = '<a type="button" class="btn btn-{class_css}" onclick="{fn}" title="{title}"><i class="fa fa-{ico}"></i></a>'
        self.list_btn = []
        self.sep = options.pop('sep','&nbsp') 
        
    def href(self, link, class_css, ico, title=''):
        self.list_btn.append(self.template.format(link=link,class_css=class_css,ico=ico,title=title))

    def click(self, fn, class_css, ico, title=''):
        self.list_btn.append(self.template_btn.format(fn=fn,class_css=class_css,ico=ico,title=title))

    def __str__(self):
        return self.sep.join(self.list_btn)