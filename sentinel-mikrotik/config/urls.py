"""sentinel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf import settings

from common.views import SwaggerSchemaView
from common.urls import urlpatterns as router_common
from utp.urls import router as router_utp
from utp.views import updateCpeUtp, updateProfile
from dashboard.urls import router as router_dashboard
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_utp.urls)), 
    re_path(r'^api/(?P<version>[-\w]+)/', include(router_dashboard.urls)),  
    re_path(r'^api/(?P<version>[-\w]+)/update_list_cpe', updateCpeUtp),
    re_path(r'^api/(?P<version>[-\w]+)/update_list_radiusprofile', updateProfile),
    path('api/docs', SwaggerSchemaView.as_view()),
    path('', include(router_common)),   
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import sys
    if "debug_toolbar" in sys.modules:
        import debug_toolbar

        urlpatterns = [
                          path('__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns