import datetime 
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.db.models import Q#, Value, Sum, F
#from django.db.models.functions import Concat
#from django.db.models.fields import IntegerField

from common.utils import timedelta_format
from common.mixins import DataTable, ButtonDataTable
#from dashboard.models import TaskModel
from utp.models import Mikrotik, CpeUtp, Plan, SystemProfile,ProfileUTP, Radius

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()


class DatatableUTP(DataTable):
    model = Mikrotik
    id_current = 0
    columns = ['id','alias','ip','cpeutps','uptime','model.model','description','radius','botones']#]
    order_columns = ['id','alias','ip','cpeutps','uptime','model.model','description','radius']
    max_display_length = 1000

    def display_id(self, obj):
        self.id_current += 1
        return f'<a href="#/devices/utp/{obj.uuid}/" data-toggle="ajax">{self.id_current}</a>'     

    def display_alias(self, obj):
        return f'<a href="#/devices/utp/{obj.uuid}/" data-toggle="ajax">{obj.alias}</a>'       

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_utp('%s')" % str(row.uuid))


    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        else:
            return super().render_column(row, column)

    def display_uptime(self, obj):
        display = '<i class="fas fa-circle" style="color: green;"></i> '
        if obj.uptime:
            u = timezone.now() - obj.uptime
            display += timedelta_format(u)
        elif obj.get_uptime():
            u = timezone.now() - obj.get_uptime()
            display += timedelta_format(u)
        return display 
            
        diff = relativedelta(obj.time_uptime , timezone.now())
        diff = '%sd, %sh, %sm, %ss' % (diff.days, diff.hours, diff.minutes, diff.seconds)
        diff = diff.replace('-','')
        return '<i class="fas fa-circle" style="color: red;"></i> conexión pérdida hace ' + str(diff)

    def display_cpeutps(self, obj):
        return '%s/%d' % (obj.cpeutp_active, obj.cpeutps)

    def display_radius(self,obj):
        print(obj.radius)
        try:
            return obj.radius.alias
        except:
            return ""

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(alias__icontains=search) |
                Q(ip__icontains=search) |
                Q(description__icontains=search) |
                Q(model__model__icontains=search)
            )
        return qs


class DatatableCPEUTP(DataTable):
    model = CpeUtp
    id_current = 0
    columns = ['id','service_number', 'status', 'uptime', 'model.brand','model.model',
              'description','mac','ip','firmware']
    order_columns = columns
    max_display_length = 1000
    html_panel = '<a data-click="panel-lateral" data-id="%s" data-panel="panel_utpcpe_content">%s</a>'
    tempuuid = ""

    def display_id(self, obj):
        self.id_current += 1
        self.tempuuid = obj.uuid
        return self.html_panel % (obj.uuid,self.id_current)

    def display_service_number(self,obj):
        if obj.service_number == None:
            return ""
        else:
            return self.html_panel % (obj.uuid,obj.service_number)

    def display_firmware(self,obj):
        if obj.firmware == None:
            return ""
        else:
            return self.html_panel % (obj.uuid, obj.firmware)

    def get_initial_queryset(self):
        try:
            #Corregido error que se traía todo los cpe: utp -> nodo_utp
            qs = self.model.objects.filter(status_field=True,
                                           nodo_utp__uuid=self.uuid)
        except:
            qs = self.model.objects.filter(status_field=True)

        filter_status = self._querydict.get('filter_status', None)
        filter_model = self._querydict.get('filter_model', None)
        filter_utp = self._querydict.get('filter_utp', None)

        if filter_status and filter_status == 'ONLINE':
            qs = qs.filter(status=True)

        if filter_status and filter_status == 'OFFLINE':
            qs = qs.filter(status=False)

        if filter_model and filter_model !='Modelo':
            qs = qs.filter(model__pk=filter_model)

        if filter_utp and filter_utp != '0':
            try:
                #Corregido error que se traía todo los cpe: utp -> nodo_utp
                qs = qs.filter(nodo_utp__uuid=filter_utp)
            except Exception as e:
                pass

        return qs

    def display_uptime(self, obj):
        try:
            if obj.status:
                u = timezone.now() - obj.uptime
                return self.html_panel % (obj.uuid, timedelta_format(u))
            return None
        except Exception as e:
            return None

    def display_RX(self, obj):
        click_template = 'data-click="panel-lateral" data-id="{id}" data-panel="panel_utpcpe_content"'
        bar_template = """<div {click} class="progress" style="height: 10px; width:51%;">
        <div onclick="openPanel('{id}');panel_utpcpe_content('#panel_right_content','{id}');" class="progress-bar {color}" style="width: {percent}%;"></div>
        </div><div {click} style="float: right; position:relative; top: -13px;">
        &nbsp;{signal}</div>"""
        signal = float(obj.RX) if obj.RX else ''
        percent = 0
        color_bar = ''

        if signal != '':
            if (signal > -10):
                html = """<div style="color:red;" data-click="panel-lateral" data-id="{}" 
                data-panel="panel_utpcpe_content">Sobrecarga {}</div>"""
                return html.format(obj.uuid,str(signal) + ' dBm')
            elif signal == -10:
                color_bar = 'bg-lime'
                percent = 100
            elif (signal < -10) & (signal >= -18.9):
                percent = 75
                color_bar = 'bg-green-transparent-8'
            elif (signal < -18.9) & (signal >= -22) :
                color_bar = 'bg-green-transparent-8 '
                percent = 55
            elif (signal < -22) & (signal >= -24.9):
                color_bar = 'bg-red-transparent-8 '
                percent = 35
            else:
                color_bar = 'bg-red'
                percent = 5

        return bar_template.format(click=click_template.format(id=obj.uuid),
                                   id=obj.uuid, color=color_bar, percent=percent,
                                   signal=str(signal) + ' dBm')

    def display_brand(self,obj):
        try:
            #return obj.brand
            bd = obj.brand
            return self.html_panel % (self.tempuuid, bd)
        except:
            return ""

    def display_model(self,obj):
        try:
            #return obj.model
            md = obj.model
            return self.html_panel % (self.tempuuid, md)
        except:
            return ""

    def display_description(self, obj):
        return  self.html_panel % (obj.uuid, obj.description)

    def display_ip(self, obj):
        if obj.active_ip != "" and obj.active_ip != None:
            if obj.ip == obj.active_ip:
                return '<p style="color: green;">{}</p>'.format(self.html_panel % (obj.uuid, obj.active_ip))
            else:
                return '<p style="color: red;">{}</p>'.format(self.html_panel % (obj.uuid, obj.active_ip))
        return '<p style="color: red;">{}</p>'.format(self.html_panel % (obj.uuid, obj.ip))

    def display_service_number(self, obj):
        return  self.html_panel % (obj.uuid, obj.service_number)

    def display_mac(self, obj):
        if obj.active_mac != "" and obj.active_mac != None:
            if obj.mac == obj.active_mac:
                return '<p style="color: green;">{}</p>'.format(self.html_panel % (obj.uuid, obj.active_mac))
            else:
                return '<p style="color: red;">{}</p>'.format(self.html_panel % (obj.uuid, obj.active_mac))
        return '<p style="color: red;">{}</p>'.format(self.html_panel % (obj.uuid, obj.mac))

    def display_status(self, obj):
        if obj.status:
            string = '<i class="fas fa-circle" style="color: green;"></i>'
            return self.html_panel % (obj.uuid, string)
        string = '<i class="fas fa-circle" style="color: red;"></i>'    
        return self.html_panel % (obj.uuid, string)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(ip__icontains=search) |
                Q(description__icontains=search) |
                Q(model__model__icontains=search) |
                Q(mac__icontains=search.replace(':',''))
            )

        filter_status = self._querydict.get('filter_status', None)
        filter_model = self._querydict.get('filter_model', None)
        

        if filter_status and filter_status == 'ONLINE':
            qs = qs.filter(status=True)

        if filter_status and filter_status == 'OFFLINE':
            qs = qs.filter(status=False)

        if filter_model and filter_model !='Modelo':
            qs = qs.filter(model__pk=filter_model)
                
        return qs


class DatatableSystemProfile(DataTable):
    model = SystemProfile
    id_current = 0
    columns = ['id','name','limitation_name','download_speed','upload_speed',
                'provisioning_available','description','botones']
    order_columns = ['id','name','limitation_name','download_speed','upload_speed',
                'provisioning_available','description']
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            m = '<i data-placement="right" data-toggle="tooltip" title="Editar Disponible Provisión" data-html="true"><a type="button" class="btn btn-light" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a></i>'
            #<img style="width:18px;" src="/static/img/papelera.png">
            if row.name:
                name=row.name
            else:
                name=""

            btn.list_btn.append(
                m.format(fn="speedUtpProvisioning(%d,'%s')" % (row.id, name))
            )
            btn.click("deleteSpeed(%d,'%s')" % (row.id,str(name)) , 'danger', 'recycle')

            return btn()
        else:
           return super().render_column(row, column)

    def display_id(self, obj):
        self.id_current += 1
        return self.id_current

    def display_provisioning_available(self, obj):
        if obj.provisioning_available:
            return '<p style="color: green;">Si</p>'
        return '<p style="color: red;">No</p>'

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains = search) |
                Q(cir__icontains = search)
            )
        return qs

class DatatableProfile(DataTable):
    model = ProfileUTP
    id_current = 0
    columns = ['id','name','limitation_name','download_speed','upload_speed',
                'provisioning_available','description','system_profile','botones']
    order_columns = ['id','name','limitation_name','download_speed','upload_speed',
                'provisioning_available','description','system_profile']
    max_display_length = 1000

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            t = '<i data-placement="right" data-toggle="tooltip" title="Cambiar System Profile" data-html="true"><a type="button" class="btn btn-info" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a></i>'
            m = '<i data-placement="right" data-toggle="tooltip" title="Editar Disponible Provisión" data-html="true"><a type="button" class="btn btn-light" onclick="{fn}"><i class="fas fa-exchange-alt"></i></a></i>'
            #<img style="width:18px;" src="/static/img/papelera.png">
            speeds_system = row.system_profile.id if row.system_profile else 0
            if row.name:
                name=row.name
            else:
                name=""

            btn.list_btn.append(
                m.format(fn="speedUtpProvisioning(%d,'%s')" % (row.id, name))
            )
            btn.list_btn.append(
                t.format(fn="speedUtpSystem(%d,%d,'%s')" % (row.id, speeds_system, name))
            )
            btn.click("deleteSpeed(%d,'%s')" % (row.id,str(name)) , 'danger', 'recycle')

            return btn()
        else:
           return super().render_column(row, column)

    def display_id(self, obj):
        self.id_current += 1
        return self.id_current

    def display_system_profile(self,obj):
        if obj.system_profile:
            return obj.system_profile.name
        else:
            return ""

    def display_provisioning_available(self, obj):
        if obj.provisioning_available:
            return '<p style="color: green;">Si</p>'
        return '<p style="color: red;">No</p>'

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains = search) |
                Q(cir__icontains = search)
            )
        return qs


class DatatablePlan(DataTable):
    model = Plan 
    columns = ['id', 'operator','name','type_plan','profile_name.download_speed','profile_name.upload_speed',
                'aggregation_rate','profile_name','matrix_plan','botones']
    order_columns = columns
    max_display_length = 1000

    def display_profile_name(self,obj):
        if obj.profile_name.name:
            return obj.profile_name.name
        return ''

    def display_aggregation_rate(self, obj):
        if obj.aggregation_rate:
            return "1:%d" % obj.aggregation_rate
        return ''

    def display_upload_speed(self, obj):
        if obj.upload_speed:
            return "%d Mbps" % obj.upload_speed
        return ''

    def display_download_speed(self, obj):
        if obj.download_speed:
            return "%d Mbps" % obj.download_speed
        return ''

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(name__icontains = search) |
                Q(type_plan__name__icontains = search)
            )
        return qs

    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.click("editPlan('%s')" % str(row.id) , 'info', 'edit')
            #return btn()
            return ""
        else:
            return super().render_column(row, column)

class DatatableRadius(DataTable):
    model = Radius
    id_current = 0
    columns = ['id','alias','ip','uptime','model.model','description','botones']#]
    order_columns = ['id','alias','ip','uptime','model.model','description']
    max_display_length = 1000

    def display_id(self, obj):
        self.id_current += 1
        return f'<a href="#/devices/utp/radius/{obj.uuid}/" data-toggle="ajax">{self.id_current}</a>'     
        #return self.id_current

    def display_alias(self, obj):
        return f'<a href="#/devices/utp/radius/{obj.uuid}/" data-toggle="ajax">{obj.alias}</a>'   
        #return obj.alias

    def display_model(self,obj):
        try:
            return obj.model
        except:
            return ""  

    def btn_delete(self, row):
        t = """<a type="button" class="btn btn-danger" onclick="{fn}">
        <img style="width:18px;" src="/static/img/papelera.png"></a>"""
        return t.format(fn="delete_utpradius('%s')" % str(row.uuid))


    def render_column(self, row, column):
        if column == 'botones':
            btn = ButtonDataTable()
            btn.list_btn.append(self.btn_delete(row))
            return btn()
        else:
            return super().render_column(row, column)

    def display_uptime(self, obj):
        display = '<i class="fas fa-circle" style="color: green;"></i> '
        if obj.uptime:
            u = timezone.now() - obj.uptime
            display += timedelta_format(u)
        elif obj.get_uptime():
            u = timezone.now() - obj.get_uptime()
            display += timedelta_format(u)
        return display 
            
        diff = relativedelta(obj.time_uptime , timezone.now())
        diff = '%sd, %sh, %sm, %ss' % (diff.days, diff.hours, diff.minutes, diff.seconds)
        diff = diff.replace('-','')
        return '<i class="fas fa-circle" style="color: red;"></i> conexión pérdida hace ' + str(diff)

    def filter_queryset(self, qs):
        search = self.request.POST.get(u'search[value]', None)
        if search:
            qs = self.model.objects.filter(
                Q(alias__icontains=search) |
                Q(ip__icontains=search) |
                Q(description__icontains=search) #|
                #Q(model__model__icontains=search)
            )
        return qs