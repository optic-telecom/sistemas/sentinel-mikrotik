import uuid
from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator
from django.utils import timezone
from common.utils import timedelta_format

from common.models import BaseModel


class Device(BaseModel):
    uuid = models.UUIDField(db_index=True,
                            default=uuid.uuid4,
                            editable=settings.DEBUG)
    class Meta:
        abstract = True


class ModelDevice(BaseModel):
    model = models.CharField(max_length=80)
    brand = models.CharField(max_length=80, null=True, blank=True)

    def __str__(self):
        return self.model

    class Meta:
        ordering = ['-id']



class Radius(Device):
    alias = models.CharField(max_length=80, db_index=True)
    ip = models.CharField(max_length=15, db_index=True)
    mac_eth1 = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    status = models.BooleanField(default=False)
    model = models.ForeignKey(ModelDevice,
                              on_delete=models.CASCADE)
    description = models.TextField()
    uptime = models.DateTimeField(null=True, blank=True)
    time_uptime = models.DateTimeField(null=True, blank=True)
    def __str__(self):
        if self.ip:
            return f"{self.alias} {self.ip}"
        return self.alias

    def get_uptime(self):
        return self.uptime

    def set_status_offline(self):
        self.status = False
        self.save()

    def set_status_online(self):
        self.status = True
        self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None
            
    def save(self, *args, **kwargs):
        super(Radius, self).save(*args, **kwargs)

    class Meta:
        ordering = ["alias"]
        unique_together = ("alias", "ip")


class RadiusConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    radius = models.ForeignKey(Radius, blank=True, on_delete=models.CASCADE)
    connections = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.radius.alias

    def release_connection(self):
        if self.connections > 0:
            self.connections -= 1
            self.save()

    def connection_usage(self):
        self.connections += 1
        self.save()

    def get_ip(self):
        return self.radius.ip

    class Meta:
        ordering = ["username"]



class Mikrotik(Device):
    name = models.CharField(max_length=80,blank=True)
    alias = models.CharField(max_length=80, db_index=True)
    ip = models.CharField(max_length=15, db_index=True)
    mac_eth1 = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    status = models.BooleanField(default=False)
    model = models.ForeignKey(ModelDevice,
                              on_delete=models.CASCADE)
    architecture = models.CharField(max_length=80,blank=True)
    description = models.TextField()
    firmware = models.CharField(max_length=100, null=True, blank=True)
    serial = models.CharField(max_length=80,blank=True)
    cpeutps = models.PositiveSmallIntegerField(default=0)
    cpeutp_active = models.PositiveSmallIntegerField(default=0)
    ram = models.CharField(max_length=10, null=True, blank=True)
    cpu = models.CharField(max_length=10, null=True, blank=True)
    storage = models.CharField(max_length=10, null=True, blank=True)
    temperature_cpu = models.CharField(max_length=10, null=True, blank=True)
    temperature = models.CharField(max_length=10, null=True, blank=True)
    voltage = models.PositiveSmallIntegerField(default=0)
    power_consumption = models.PositiveSmallIntegerField(default=0)
    uptime = models.DateTimeField(null=True, blank=True)
    time_uptime = models.DateTimeField(null=True, blank=True)
    comment = models.TextField(max_length=300,null=True)
    radius = models.ForeignKey(Radius, null=True, on_delete=models.SET_NULL)
    
    def __str__(self):
        if self.ip:
            return f"{self.alias} {self.ip}"
        return self.alias

    def get_uptime(self):
        return self.uptime

    def set_status_offline(self):
        self.status = False
        self.save()

    def set_status_online(self):
        self.status = True
        self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None

    @property
    def get_uptime_date(self):
        return self.uptime.strftime('Desde %d/%m/%Y a las %H:%M')

    @property
    def get_radius_alias(self):
        if self.radius:
            return self.radius.alias
        else:
            return None
    

    def save(self, *args, **kwargs):
        super(Mikrotik, self).save(*args, **kwargs)

    class Meta:
        ordering = ["alias"]
        unique_together = ("alias", "ip")


class MikrotikConnection(BaseModel):
    username = models.CharField(max_length=25)
    password = models.CharField(max_length=30)
    mikrotik = models.ForeignKey(Mikrotik, blank=True, on_delete=models.CASCADE)
    connections = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.mikrotik.alias

    def release_connection(self):
        if self.connections > 0:
            self.connections -= 1
            self.save()

    def connection_usage(self):
        self.connections += 1
        self.save()

    def get_ip(self):
        return self.mikrotik.ip

    class Meta:
        ordering = ["username"]



class CpeUtp(Device):

    alias = models.CharField(max_length=80, db_index=True, null=True, blank=True)
    serial = models.CharField(max_length=30, db_index=True)
    nodo_utp = models.ForeignKey(Mikrotik, on_delete=models.CASCADE)
    description = models.TextField()
    model = models.ForeignKey(ModelDevice, null=True, blank=True,
                              on_delete=models.CASCADE)
    service_number = models.PositiveSmallIntegerField(null=True, blank=True)
    service_number_verified = models.BooleanField(default=False)
    # Quitada la posibilidad de que status sea NULL en este campo para evitar error en el serializer
    status = models.BooleanField(default=False, blank=True)
    uptime = models.DateTimeField(null=True, blank=True)
    mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    active_mac = models.CharField(max_length=17, db_index=True, null=True, blank=True)
    ip = models.GenericIPAddressField(protocol="IPv4", db_index=True, null=True, blank=True)
    active_ip = models.GenericIPAddressField(protocol="IPv4", db_index=True, null=True, blank=True)
    firmware = models.CharField(max_length=100, null=True, blank=True)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    

    def delete_this(self):
        self.delete()

    def __str__(self):
        if self.alias:
            return self.alias
        return self.serial

    def set_status_offline(self):
        self.status = False
        self.save()

    @property
    def get_uptime_delta(self):
        try:
            if self.status:
                u = timezone.now() - self.uptime
                return timedelta_format(u)
            return None
        except Exception as e:
            print (e)
            return None

    @property
    def get_uptime_date(self):
        return self.uptime.strftime('Desde %d/%m/%Y a las %H:%M')
  

    def get_mac(self):
        return self.mac

    class Meta:
        ordering = ["serial"]
        unique_together = (("alias", "nodo_utp"),)


class Operator(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=3)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class SystemProfile(BaseModel):
    name = models.CharField(max_length=50, unique=True)
    limitation_name = models.CharField(max_length=50)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    
    provisioning_available = models.BooleanField(default=False)
    description = models.TextField(blank=True,default="")

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class ProfileUTP(BaseModel):
    name = models.CharField(max_length=50)
    limitation_name = models.CharField(max_length=50)
    download_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)
    upload_speed = models.PositiveSmallIntegerField(validators=[MaxValueValidator(1000)], null=True, blank=True)    
    provisioning_available = models.BooleanField(default=False)
    description = models.TextField(blank=True,default="")
    radius = models.ForeignKey(Radius, on_delete=models.CASCADE)
    system_profile = models.ForeignKey(SystemProfile, on_delete = models.SET_NULL,
                                     null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class TypePLAN(BaseModel):
    name = models.CharField(max_length=100,unique=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["-name"]


class Plan(BaseModel):

    operator = models.ForeignKey(Operator, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    type_plan = models.ForeignKey(TypePLAN, on_delete=models.CASCADE)
    aggregation_rate = models.PositiveSmallIntegerField(validators=[MaxValueValidator(4096)], null=True, blank=True)   
    profile_name = models.ForeignKey(SystemProfile, on_delete=models.CASCADE)
    matrix_plan = models.PositiveSmallIntegerField()                                       

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        ordering = ["-matrix_plan"]