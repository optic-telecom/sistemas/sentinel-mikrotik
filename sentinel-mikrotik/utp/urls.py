from rest_framework import routers
from .views import *

router = routers.DefaultRouter()

router.register(r'utp', UTPViewSet, 'utp_api')
router.register(r'cpeutp', CpeUtpViewSet, 'cpeutp_api')
router.register(r'operator', OperatorViewSet, 'operator_api')
router.register(r'profileutp', ProfileViewSet, 'profile_utp_api')
router.register(r'system_profileutp', SystemProfileViewSet, 'system_profile_utp_api')
router.register(r'type_plan', TypePLANViewSet, 'type_plan_api')
router.register(r'plan', PlanViewSet, 'plan_api')
router.register(r'radius', RadiusViewSet, 'radiusutp_api')