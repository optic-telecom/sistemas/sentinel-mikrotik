# Generated by Django 2.1.3 on 2019-07-10 15:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('utp', '0003_auto_20190709_0208'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalmikrotik',
            name='firmware',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='historicalmikrotik',
            name='name',
            field=models.CharField(blank=True, max_length=80),
        ),
        migrations.AddField(
            model_name='historicalmikrotik',
            name='status',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mikrotik',
            name='firmware',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='mikrotik',
            name='name',
            field=models.CharField(blank=True, max_length=80),
        ),
        migrations.AddField(
            model_name='mikrotik',
            name='status',
            field=models.BooleanField(default=False),
        ),
    ]
