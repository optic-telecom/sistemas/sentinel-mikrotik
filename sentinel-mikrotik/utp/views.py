#from django.contrib.auth import get_user_model
from django_filters.rest_framework import DjangoFilterBackend
from django.views.decorators.csrf import csrf_exempt
from dynamic_preferences.registries import global_preferences_registry

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.generics import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import list_route
from rest_framework.renderers import JSONRenderer

from .serializers import MikrotikSerializer, CPEUTPSerializer, \
                            OperatorSerializer, ProfileUTPSerializer, \
                            TypePLANSerializer, PlanSerializer, RadiusSerializer, \
                            main_updatecpeutp, main_updateprofileutp, \
                            SystemProfileSerializer

from .models import Mikrotik, MikrotikConnection,ModelDevice, CpeUtp, Operator, \
                    TypePLAN, Plan, ProfileUTP, Radius, RadiusConnection, SystemProfile
from .mikrotik import get_mikrotik_leases, delete_radius_profile_witheverything, \
                      delete_radius_profile_onlylink

from dashboard.utils import LOG


__all__ = ['UTPViewSet','CpeUtpViewSet','OperatorViewSet','ProfileViewSet',
            'TypePLANViewSet','PlanViewSet','RadiusViewSet','updateCpeUtp',
            'updateProfile','SystemProfileViewSet']

global_preferences = global_preferences_registry.manager()

_l = LOG()

class UTPViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Mikrotik
    retrieve:
        End point for Mikrotik
    update:
        End point for Mikrotik
    delete:
        End point for Mikrotik
    """
    queryset = Mikrotik.objects.all()
    serializer_class = MikrotikSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        for_alias = self.request.GET.get('alias', None)
        if for_alias:
            return Mikrotik.objects.filter(alias__icontains=for_alias)
        return Mikrotik.objects.filter()


class CpeUtpViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for CPEUTP
    retrieve:
        End point for CPEUTP
    update:
        End point for CPEUTP
    delete:
        End point for CPEUTP
    """
    queryset = CpeUtp.objects.all()
    serializer_class = CPEUTPSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'


@csrf_exempt
def updateCpeUtp(request,version):
    
    if request.method=="POST" or request.method=="GET":

        url = request.get_full_path()
        utp_uuid = url.split("/").pop()
        try:
            main_updatecpeutp(utp_uuid)
            response = Response(
            {"detail": "ok"},
            content_type="application/json",
            status=status.HTTP_200_OK,
            )            
        except Exception as e:
            response = Response({**e.detail},
            content_type="application/json",
            status=status.HTTP_400_BAD_REQUEST,
            )  
        
        
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response


@csrf_exempt
def updateProfile(request,version):
    
    if request.method=="POST" or request.method=="GET":
        url = request.get_full_path()
        radiusid = url.split("/").pop()
        main_updateprofileutp(radiusid)
        response = Response(
            {"detail": "ok"},
            content_type="application/json",
            status=status.HTTP_200_OK,
        )
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response
    else:
        response = Response(
            {"detail": "ok"},
            content_type="application/json",
            status=status.HTTP_200_OK,
        )
        response.accepted_renderer = JSONRenderer()
        response.accepted_media_type = "application/json"
        response.renderer_context = {}
        return response


class OperatorViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Operator
    retrieve:
        End point for Operator
    update: 
        End point for Operator
    delete:
        End point for Operator
    """
    queryset = Operator.objects.all()
    serializer_class = OperatorSerializer

class SystemProfileViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Profile
    retrieve:
        End point for Profile
    update:
        End point for Profile
    delete:
        End point for Profile
    """
    queryset = SystemProfile.objects.all()
    serializer_class = SystemProfileSerializer 

class ProfileViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Profile
    retrieve:
        End point for Profile
    update:
        End point for Profile
    delete:
        End point for Profile
    """
    queryset = ProfileUTP.objects.all()
    serializer_class = ProfileUTPSerializer  

    def destroy(self, request, *args, **kwargs):
        url = request.path.split("/")
        id_given = url.pop()
        deleteall = False
        if id_given=="":
            id_given = url.pop()
        if "?all" in id_given:
            deleteall = True
            id_given = int(url.pop())
        else:
            id_given = int(id_given)
        elem = ProfileUTP.objects.get(id=id_given)
        radius = Radius.objects.get(id=elem.radius.id)
        connection_data = RadiusConnection.objects.get(radius=radius)
        user = connection_data.username
        _l.PROFILEUTP.ADD(by_representation=user, id_value=elem.id,
                description=f'Petición para borrar profile', entity_representation=repr(elem))
        data = {
            "profile" : elem.name,
            "limitation" : elem.limitation_name
        }
        try:
            if deleteall:
                _l.RADIUS.ADD(by_representation=user, id_value=radius.uuid,
                    description=f'Petición para borrar profile link de RADIUS incluyendo profile y limitation',
                    entity_representation=repr(radius))
                try:
                    result = delete_radius_profile_witheverything(radius.ip,user,connection_data.password,data)
                    if result != {}:
                        raise Exception("Error: "+str(result))
                except Exception as e:
                    _l.PROFILEUTP.ADD(by_representation=user, id_value=radius, id_field='id',
                    description=f'fallo borrar profile: {e}')

                    raise serializers.ValidationError({
                      "error" : f'fallo borrar profile: {e}',
                      "detail" : str(e)
                    })
            else:
                _l.RADIUS.ADD(by_representation=user, id_value=radius.uuid,
                    description=f'Petición para borrar profile link de RADIUS sin borrar profile y limitation',
                    entity_representation=repr(radius))
                try:
                    result = delete_radius_profile_onlylink(radius.ip,user,connection_data.password,data)
                    if result != {}:
                        raise Exception("Error: "+str(result))
                except Exception as e:
                    _l.PROFILEUTP.ADD(by_representation=user, id_value=radius, id_field='id',
                    description=f'fallo borrar profile: {e}')

                    raise serializers.ValidationError({
                      "error" : f'fallo borrar profile: {e}',
                      "detail" : str(e)
                    })
            return super(ProfileViewSet, self).destroy(request, *args, **kwargs) 
        except Exception as e:
            _l.PROFILEUTP.ADD(by_representation=user, id_value=radius, id_field='id',
                description=f'fallo borrar profile: {e}')

            raise serializers.ValidationError({
              "error" : f'fallo borrar profile: {e}',
              "detail" : str(e)
            })


class TypePLANViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for TypePLAN
    retrieve:
        End point for TypePLAN
    update:
        End point for TypePLAN
    delete:
        End point for TypePLAN
    """ 
    queryset = TypePLAN.objects.all()
    serializer_class = TypePLANSerializer   


class PlanViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Plan
    retrieve:
        End point for Plan
    update:
        End point for Plan
    delete:
        End point for Plan
    """
    queryset = Plan.objects.all()
    serializer_class = PlanSerializer


class RadiusViewSet(viewsets.ModelViewSet):
    """
    create:
        End point for Radius
    retrieve:
        End point for Radius
    update:
        End point for Radius
    delete:
        End point for Radius
    """
    queryset = Radius.objects.all()
    serializer_class = RadiusSerializer
    lookup_field = 'uuid'
    lookup_url_kwarg = 'uuid'

    def get_queryset(self):
        for_alias = self.request.GET.get('alias', None)
        if for_alias:
            return Radius.objects.filter(alias__icontains=for_alias)
        return Radius.objects.filter()
