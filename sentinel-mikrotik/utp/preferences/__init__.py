#from decimal import Decimal

from dynamic_preferences.types import BooleanPreference, StringPreference,\
    DecimalPreference, IntegerPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry
#from dynamic_preferences.users.registries import user_preferences_registry

# we create some section objects to link related preferences together
general = Section('general')


@global_preferences_registry.register
class SiteTitle(StringPreference):
    section = general
    name = 'title'
    default = 'Mikrotik'
    required = False


# @global_preferences_registry.register
# class URLFeedback(StringPreference):
#     name = 'url_slack_feedback'
#     default = "https://hooks.slack.com/services/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"
#     required = False
