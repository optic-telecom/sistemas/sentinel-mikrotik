from django.contrib import admin, messages
from common.admin import BaseAdmin
from .models import Mikrotik, ModelDevice, CpeUtp, Operator, ProfileUTP,\
    TypePLAN,Plan, Radius, SystemProfile

from dynamic_preferences.registries import global_preferences_registry
global_preferences = global_preferences_registry.manager()


@admin.register(Mikrotik)
class MikrotikAdmin(BaseAdmin):
    list_display = ('ip','alias','model', 'description',
    	 			'cpeutps','cpeutp_active','uptime')

@admin.register(ModelDevice)
class ModelDeviceAdmin(BaseAdmin):
    list_display = ('model',)


@admin.register(CpeUtp)
class CpeUtpAdmin(BaseAdmin):
    list_display = ('ip','alias','model', 'description','uptime')
    list_filter = ('nodo_utp','model') 

@admin.register(Operator)
class OperatorAdmin(BaseAdmin):
    list_display = ('name','code')

@admin.register(SystemProfile)
class SystemProfileAdmin(BaseAdmin):
    list_display = ('name','download_speed','upload_speed','provisioning_available',
                    'limitation_name','description')

@admin.register(ProfileUTP)
class ProfileUTPdmin(BaseAdmin):
    list_display = ('name','download_speed','upload_speed','provisioning_available',
                    'limitation_name','description')

@admin.register(TypePLAN)
class TypePLANAdmin(BaseAdmin):
    list_display = ('name',)

@admin.register(Plan)
class PlanAdmin(BaseAdmin):
    list_display = ('name','operator','profile_name','matrix_plan',)
    list_filter = ('type_plan',)  

@admin.register(Radius)
class RadiusAdmin(BaseAdmin):
    list_display = ('ip','alias','model', 'description','uptime','status')