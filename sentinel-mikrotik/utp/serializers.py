import uuid
from datetime import timedelta
from django.conf import settings
from django.utils import timezone
from django.core.management import call_command

from rest_framework import serializers
from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework.validators import UniqueValidator
from rest_framework.utils import model_meta

from common.serializers import QueryFieldsMixin

from .models import Mikrotik, ModelDevice, MikrotikConnection,CpeUtp, Operator, ProfileUTP, TypePLAN, \
                    Plan, Radius, RadiusConnection, SystemProfile
from utp.mikrotik import get_mikrotik_info,get_mikrotik_leases,create_radius_profile,get_radius_profile
from dashboard.utils import LOG
    
from dashboard.models import TaskModel    

__all__ = ['MikrotikSerializer', 'ModelDeviceSerializer','CPEUTPSerializer',
            'OperatorSerializer','ProfileUTPSerializer','TypePLANSerializer',
            'PlanSerializer','RadiusSerializer','ProvisioningSerializer']

_l = LOG()

class ModelDeviceSerializer(HyperlinkedModelSerializer):

    class Meta:
        model = ModelDevice
        fields = ('id', 'model','brand')


class ListNodeField(serializers.Field):
    def to_internal_value(self, obj):
        if isinstance(obj, list):
            return obj[0]
        else:
            msg = self.error_messages['invalid']
            raise ValidationError(msg)


class RadiusSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    model = ModelDeviceSerializer(read_only=True)
    user = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = self.context['request'].user
        username = validated_data.pop('user')
        password = validated_data.pop('password')
        ip = validated_data.get('ip')
        id_data = uuid.uuid4()


        try:
            info = get_mikrotik_info(ip,username,password)
            if (not isinstance(info,dict)) or info=={}:
                if info == {}:
                    msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                            y que el IP Services permita la conexión desde las IP 190.113.247.215 y 190.113.247.219.'
                else:
                    msg = info
                raise Exception('{}'.format(msg))
            model_name = info['model']
            try:
                model = ModelDevice.objects.get(model=model_name)
            except ModelDevice.DoesNotExist:
                model = ModelDevice.objects.create(model=model_name,id_data=id_data,brand=info['brand'])

                _l.MODEL.ADD(by_representation=user, id_value=model.id,
                description=f'Modelo creado por crear RADIUS', entity_representation=repr(model))
            except Exception as e:
                print ("Error getmodel serializer RADIUS")
                print (e)
                raise serializers.ValidationError({
                  "error" : "No se puede obtener el modelo del dispositivo",
                  "detail" : str(e)
                })                      
            
            validated_data['model'] = model
            # If week
            if "w" in info['uptime']:
                weeks,rest = info['uptime'].split('w')
                weeks=int(weeks)
            else:
                weeks=0
                rest = info['uptime']
            # If day
            if "d" in rest:
                days,rest = rest.split('d')
                days=int(days)
            else:
                days=0
            # If hour
            if "h" in rest:
                hours,rest = rest.split('h')
                hours=int(hours)
            else:
                hours=0
            # If minute
            if "m" in rest:
                minutes,rest = rest.split('m')
                minutes=int(minutes)
            else:
                minutes=0
            # If seconds
            if "s" in rest:
                secs,rest = rest.split('s')
                secs=int(secs)
            else:
                secs=0
            # Get uptime with retrieved values
            value_uptime = timezone.now() - timedelta(weeks=weeks, days=days, hours=hours,
                                                       minutes=minutes, seconds=secs)

            obj = Radius.objects.create(**validated_data)
            print("here")
            obj.status = True  
            obj.uptime = value_uptime
            obj.time_uptime = timezone.now()
            obj.ip = ip
            obj.description = info['description']
            obj.name = ""
            obj.save()

            _l.RADIUS.ADD(by_representation=user, id_value=obj.uuid,
                description=f'Nuevo RADIUS', entity_representation=repr(obj))


            RadiusConnection.objects.create(radius=obj,
                                         username=username,
                                         password=password,
                                         id_data=id_data)

            """ task """             
            TaskModel.objects.create(
              id_obj = obj.id,
              model_obj = 'radius',
              uuid = obj.uuid,
              name = 'task_new_radius',
              id_data = id_data
            )
            return obj

        except Exception as e:
            _l.RADIUS.ADD(by_representation=user, id_value=ip, id_field='ip',
            description=f'fallo crear: {e}')

            raise serializers.ValidationError({
              "error" : f'fallo crear: {e}',
              "detail" : str(e)
            })

    class Meta:
        model = Radius
        fields = ('ip','alias','model', 'description','uptime','id',
                  'user', 'password','uuid','status','mac_eth1',
                  'get_uptime_delta')


class MikrotikSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):
    model = ModelDeviceSerializer(read_only=True)
    radius = serializers.ModelField(model_field=Radius()._meta.get_field('id'))
    user = serializers.CharField(write_only=True)
    password = serializers.CharField(write_only=True)

    def get_task_in_progress(self, obj):
        pass

    def validate(self, attrs):
        if self.context['view'].action == 'partial_update' or\
          self.context['view'].action == 'update':
          return super().validate(attrs)

        import ipaddress
        try:
            ipaddress.ip_address(attrs['ip'])
        except ValueError as e:
            _err = {'error':'La ip es invalida'}
            raise serializers.ValidationError(_err)
        
        return super().validate(attrs)

    def create(self, validated_data):
        user = self.context['request'].user
        username = validated_data.pop('user')
        password = validated_data.pop('password')
        ip = validated_data.get('ip')
        id_data = uuid.uuid4()
        radiusid = validated_data.pop('radius')


        try:
            try:
                radius = Radius.objects.get(id=radiusid)
            except:
                radius = ""
            print(radius)
            info = get_mikrotik_info(ip,username,password)
            if (not isinstance(info,dict)) or info=={}:
                if info == {}:
                    msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                            y que el IP Services permita la conexión desde las IP 190.113.247.215 y 190.113.247.219.'
                else:
                    msg = info
                raise Exception('{}'.format(msg))
            model_name = info['model']
            try:
                model = ModelDevice.objects.get(model=model_name)
            except ModelDevice.DoesNotExist:
                model = ModelDevice.objects.create(model=model_name,id_data=id_data,brand=info['brand'])

                _l.MODEL.ADD(by_representation=user, id_value=model.id,
                description=f'Modelo creado por crear Mikrotik', entity_representation=repr(model))
            except Exception as e:
                print ("Error getmodel serializer Mikrotik")
                print (e)
                raise serializers.ValidationError({
                  "error" : "No se puede obtener el modelo del dispositivo",
                  "detail" : str(e)
                })                      
            
            validated_data['model'] = model
            # If week
            if "w" in info['uptime']:
                weeks,rest = info['uptime'].split('w')
                weeks=int(weeks)
            else:
                weeks=0
                rest = info['uptime']
            # If day
            if "d" in rest:
                days,rest = rest.split('d')
                days=int(days)
            else:
                days=0
            # If hour
            if "h" in rest:
                hours,rest = rest.split('h')
                hours=int(hours)
            else:
                hours=0
            # If minute
            if "m" in rest:
                minutes,rest = rest.split('m')
                minutes=int(minutes)
            else:
                minutes=0
            # If seconds
            if "s" in rest:
                secs,rest = rest.split('s')
                secs=int(secs)
            else:
                secs=0
            # Get uptime with retrieved values
            value_uptime = timezone.now() - timedelta(weeks=weeks, days=days, hours=hours,
                                                       minutes=minutes, seconds=secs)

            obj = Mikrotik.objects.create(**validated_data)
            if radius != "":
                obj.radius = radius
            obj.status = True
            obj.firmware = info['firmware']            
            obj.uptime = value_uptime
            obj.time_uptime = timezone.now()
            obj.id_data = id_data
            obj.ip = ip
            obj.description = info['description']
            obj.name = ""
            obj.architecture = info['architecture']
            obj.serial = info['serial']
            obj.cpu = str(info['cpu'])
            obj.ram = str(info['ram'])
            obj.storage = str(info['hdd'])
            if info['temperature']!='':
                obj.temperature = info['temperature']
            if info['voltage']!='':
                obj.voltage = round(float(info['voltage']),0)
            if info['temperature_cpu']!='':
                obj.temperature_cpu = info['temperature_cpu']
            if info['power_consumption']!='':
                obj.power_consumption = round(float(info['power_consumption']),0)
            obj.save()

            _l.MIKROTIK.ADD(by_representation=user, id_value=obj.uuid,
                description=f'Nuevo Mikrotik', entity_representation=repr(obj))


            MikrotikConnection.objects.create(mikrotik=obj,
                                         username=username,
                                         password=password,
                                         id_data=id_data)

            """ task """             
            TaskModel.objects.create(
              id_obj = obj.id,
              model_obj = 'mikrotik',
              uuid = obj.uuid,
              name = 'task_new_mikrotik',
              id_data = id_data
            )

            if 'leases' in info.keys():
                listleases = info['leases']
                for i in listleases:
                    try:
                        print(i)
                        model_name = i['model']
                        try:
                            model = ModelDevice.objects.get(model=model_name)
                        except ModelDevice.DoesNotExist:
                            model = ModelDevice.objects.create(model=model_name,id_data=id_data,brand=i['brand'])
                            _l.MODEL.ADD(by_representation=user, id_value=model.id,
                            description=f'Modelo creado por crear una CPE al crear un Mikrotikik',
                            entity_representation=repr(model))
                        
                        cpe = CpeUtp.objects.create(status=i['status'],
                                                    description=i['description'],
                                                    mac=i['mac'],
                                                    active_mac=i['active_mac'],
                                                    nodo_utp=obj,
                                                    model=model,
                                                    ip=i['ip'],
                                                    active_ip=i['active_ip'],
                                                    firmware=i['firmware'],
                                                    alias=i['description']
                                                    )

                        if i['uptime'] != None:
                            # If week
                            if "w" in i['uptime']:
                                weeks,rest = i['uptime'].split('w')
                                weeks=int(weeks)
                            else:
                                weeks=0
                                rest = i['uptime']
                            # If day
                            if "d" in rest:
                                days,rest = rest.split('d')
                                days=int(days)
                            else:
                                days=0
                            # If hour
                            if "h" in rest:
                                hours,rest = rest.split('h')
                                hours=int(hours)
                            else:
                                hours=0
                            # If minute
                            if "m" in rest:
                                minutes,rest = rest.split('m')
                                minutes=int(minutes)
                            else:
                                minutes=0
                            # If seconds
                            if "s" in rest:
                                secs,rest = rest.split('s')
                                secs=int(secs)
                            else:
                                secs=0
                            # Get uptime with retrieved values
                            value_uptime = timezone.now() - timedelta(weeks=weeks, days=days, hours=hours,
                                                                       minutes=minutes, seconds=secs)
                            print(value_uptime)
                            cpe.uptime = value_uptime
                            cpe.save()
                        print(1)
                    except:
                        cpe = CpeUtp.objects.create(status=i['status'],
                                                    description=i['description'],
                                                    mac=i['mac'],
                                                    active_mac=i['active_mac'],
                                                    nodo_utp=obj,
                                                    ip=i['ip'],
                                                    active_ip=i['active_ip'],
                                                    alias=i['description']
                                                    )
                        print(2)
                    cpe.service_number=i['service_number']
                    if 'download_speed' in i.keys():
                        cpe.download_speed=i['download_speed']
                    if 'upload_speed' in i.keys():
                        cpe.upload_speed=i['upload_speed']
                    cpe.save()
                    _l.CPEUTP.ADD(by_representation=user, id_value=cpe.id,
                        description=f'CPE creado al crear Mikrotik', entity_representation=repr(cpe))
                    obj.cpeutps = obj.cpeutps + 1
                    if cpe.status == True:
                        obj.cpeutp_active = obj.cpeutp_active + 1
                obj.save()
            return obj

        except Exception as e:
            _l.MIKROTIK.ADD(by_representation=user, id_value=ip, id_field='ip',
            description=f'fallo crear: {e}')

            raise serializers.ValidationError({
              "error" : f'fallo crear: {e}',
              "detail" : str(e)
            })

    class Meta:
        model = Mikrotik
        fields = ('ip','alias','model', 'description', 'cpeutps','cpeutp_active','uptime','id',
                  'user', 'password','uuid','name','status','firmware','architecture',
                  'cpu','ram','serial','mac_eth1','storage','get_uptime_delta','get_uptime_date',
                  'temperature','temperature_cpu','voltage','power_consumption','comment',
                  'radius','get_radius_alias'
                  )


class CPEUTPSerializer(QueryFieldsMixin, HyperlinkedModelSerializer):

    nodo_utp = serializers.CharField(read_only=True, source='nodo_utp.alias')
    model = ModelDeviceSerializer()
    # Comentada la siguiente línea por error "NoneType has no attribute 'model'"
    #model = serializers.CharField(source='model.model')

    class Meta:
        model = CpeUtp
        fields = ('serial','model','description','service_number',
                  'nodo_utp','uuid','alias','status','uptime','mac','ip',
                  'firmware','nodo_utp','active_mac','active_ip',
                  'get_uptime_delta','get_uptime_date')

def extract_elem_from_list(givenlist,index):
    """
    Extracts an element from a list given its index in said list.
    This method is used in main_updatecpeutp.
    """
    if index == 0:
        return givenlist[1:]
    elif index==len(givenlist)-1:
        return givenlist[:len(givenlist)-1]
    else:
        return givenlist[:index]+givenlist[index+1:]

def update_cpeutp(cpeutp,i,obj,user):
    """
    This method supports the main for updating cpe.
    Simply grabs a dict and updates de writable fields of a CpeUtp instance.
    """
    boolean = True
    if i['service_number']!=cpeutp.service_number:
        boolean = False
        cpeutp.service_number=i['service_number']
    if i['status']!=cpeutp.status:
        boolean = False
        cpeutp.status=i['status']
        if cpeutp.status == True:
            obj.cpeutp_active += 1
            obj.save()
            _l.MIKROTIK.ADD(by_representation=user, id_value=obj.id,
                description=f'Cambiado status de un CPE al actualizar lista CPEUTP del nodo',
                entity_representation=repr(obj))
        else:
            if obj.cpeutp_active != 0:
                obj.cpeutp_active -= 1
                obj.save()
                _l.MIKROTIK.ADD(by_representation=user, id_value=obj.id,
                    description=f'Cambiado status de un CPE al actualizar lista CPEUTP del nodo',
                    entity_representation=repr(obj))
    if i['description']!=cpeutp.description:
        boolean = False
        cpeutp.description=i['description']
    if i['mac']!=cpeutp.mac:
        boolean = False
        cpeutp.mac=i['mac']
    if i['active_mac']!=cpeutp.active_mac:
        boolean = False
        cpeutp.active_mac=i['active_mac']
    if "model" in i.keys():
        boolean = False
        model_name=i['model']
        try:
            model = ModelDevice.objects.get(model=model_name)
        except ModelDevice.DoesNotExist:
            model = ModelDevice.objects.create(model=model_name,id_data=obj.uuid,brand=i['brand'])
            _l.MODEL.ADD(by_representation=user, id_value=model.id,
            description=f'Modelo creado por crear una CPE al actualizar la lista CPEUTP de un Mikrotikik',
            entity_representation=repr(model))
        if cpeutp.model!=model:
            boolean=False
            cpeutp.model=model
    else:
        if cpeutp.model:
            boolean = False
            cpeutp.model=None
    if i['ip']!=cpeutp.ip:
        boolean = False
        cpeutp.ip=i['ip']
    if i['active_ip']!=cpeutp.active_ip:
        boolean = False
        cpeutp.active_ip=i['active_ip']
    if "firmware" in i.keys() and i['firmware']!=cpeutp.firmware:
        boolean = False
        cpeutp.firmware=i['firmware']
    if "uptime" in i.keys() and i['uptime'] != None:
        # If week
        if "w" in i['uptime']:
            weeks,rest = i['uptime'].split('w')
            weeks=int(weeks)
        else:
            weeks=0
            rest = i['uptime']
        # If day
        if "d" in rest:
            days,rest = rest.split('d')
            days=int(days)
        else:
            days=0
        # If hour
        if "h" in rest:
            hours,rest = rest.split('h')
            hours=int(hours)
        else:
            hours=0
        # If minute
        if "m" in rest:
            minutes,rest = rest.split('m')
            minutes=int(minutes)
        else:
            minutes=0
        # If seconds
        if "s" in rest:
            secs,rest = rest.split('s')
            secs=int(secs)
        else:
            secs=0
        # Get uptime with retrieved values
        value_uptime = timezone.now() - timedelta(weeks=weeks, days=days, hours=hours,
                                                   minutes=minutes, seconds=secs)
        if value_uptime!=cpeutp.uptime:
            boolean = False
            cpeutp.uptime = value_uptime
    if 'download_speed' in i.keys():
        cpeutp.download_speed=i['download_speed']
        boolean=False
    if 'upload_speed' in i.keys():
        cpeutp.upload_speed=i['upload_speed']
        boolean=False
    if not boolean:
        cpeutp.save()
        _l.CPEUTP.ADD(by_representation=user, id_value=cpeutp.id,
                        description=f'CPE actualizado al actualizar lista CPEUTP de Mikrotik', entity_representation=repr(cpeutp))



def main_updatecpeutp(utp_uuid):
    """
    This is a method used in order to update the cpelist of a
    given utp node
    """
    # Get utp node
    elem = Mikrotik.objects.get(uuid=utp_uuid)
    # Get connections details for this device
    connection_data = MikrotikConnection.objects.get(mikrotik=elem)
    user = connection_data.username
    _l.MIKROTIK.ADD(by_representation=user, id_value=elem.uuid,
                description=f'Petición para actualizar lista CPEUTP de Mikrotik', entity_representation=repr(elem))
    obj = elem
    try:
        # Get device leases
        leases = get_mikrotik_leases(elem.ip,user,connection_data.password)
        if isinstance(leases,str):
            raise Exception("Error: "+leases)
        # Get cpes currently associated to utp node
        saved_cpeutp = list(CpeUtp.objects.filter(nodo_utp=elem))
        # Compare the two lists
        temp = []
        while len(leases)!=0 and len(saved_cpeutp)!=0:
            current = leases.pop()
            temp.append(current)
            print("This is current element from leases")
            print(current)
            for i in range(len(saved_cpeutp)):
                if current['ip'] == saved_cpeutp[i].ip and current['mac'] == saved_cpeutp[i].mac:
                    print("found match in saved cpeutp,updating")
                    update_cpeutp(saved_cpeutp[i],current,elem,user)
                    saved_cpeutp = extract_elem_from_list(saved_cpeutp,i)
                    print("This is the new list")
                    print(saved_cpeutp)
                    temp.pop()
                    break
        if len(leases)==0:
            leases = temp
        print(leases)
        if len(leases)!= 0 and len(saved_cpeutp)==0: # There is at least one cpe that needs to be added
            print("Need to add new cpe(s) to list")
            for i in leases:
                try:
                    print(i)
                    model_name = i['model']
                    try:
                        model = ModelDevice.objects.get(model=model_name)
                    except ModelDevice.DoesNotExist:
                        model = ModelDevice.objects.create(model=model_name,id_data=obj.uuid,brand=i['brand'])
                        _l.MODEL.ADD(by_representation=user, id_value=model.id,
                        description=f'Modelo creado por crear una CPE al actualizar la lista CPEUTP de un Mikrotikik',
                        entity_representation=repr(model))
                    
                    cpe = CpeUtp.objects.create(status=i['status'],
                                                description=i['description'],
                                                mac=i['mac'],
                                                active_mac=i['active_mac'],
                                                nodo_utp=obj,
                                                model=model,
                                                ip=i['ip'],
                                                active_ip=i['active_ip'],
                                                firmware=i['firmware'],
                                                alias=i['description']
                                                )

                    if i['uptime'] != None:
                        # If week
                        if "w" in i['uptime']:
                            weeks,rest = i['uptime'].split('w')
                            weeks=int(weeks)
                        else:
                            weeks=0
                            rest = i['uptime']
                        # If day
                        if "d" in rest:
                            days,rest = rest.split('d')
                            days=int(days)
                        else:
                            days=0
                        # If hour
                        if "h" in rest:
                            hours,rest = rest.split('h')
                            hours=int(hours)
                        else:
                            hours=0
                        # If minute
                        if "m" in rest:
                            minutes,rest = rest.split('m')
                            minutes=int(minutes)
                        else:
                            minutes=0
                        # If seconds
                        if "s" in rest:
                            secs,rest = rest.split('s')
                            secs=int(secs)
                        else:
                            secs=0
                        # Get uptime with retrieved values
                        value_uptime = timezone.now() - timedelta(weeks=weeks, days=days, hours=hours,
                                                                   minutes=minutes, seconds=secs)
                        print(value_uptime)
                        cpe.uptime = value_uptime
                        cpe.save()
                    print(1)
                except:
                    cpe = CpeUtp.objects.create(status=i['status'],
                                                description=i['description'],
                                                mac=i['mac'],
                                                active_mac=i['active_mac'],
                                                nodo_utp=obj,
                                                ip=i['ip'],
                                                active_ip=i['active_ip'],
                                                alias=i['description']
                                                )
                    print(2)
                cpe.service_number=i['service_number']
                if 'download_speed' in i.keys():
                    cpe.download_speed=i['download_speed']
                if 'upload_speed' in i.keys():
                    cpe.upload_speed=i['upload_speed']
                cpe.save()
                print(cpe)
                _l.CPEUTP.ADD(by_representation=user, id_value=cpe.id,
                    description=f'CPE creado al actualizar lista CPEUTPE de un Mikrotik', entity_representation=repr(cpe))
                obj.cpeutps+=1
                if cpe.status == True:
                    obj.cpeutp_active+=1
                obj.save()
        elif len(leases)== 0 and len(saved_cpeutp)!=0: # There is a cpe that needs to be deleted
            print("We need to delete cpeutp(s)")
            for i in saved_cpeutp:
                print("Deleting this element")
                print(i)
                print(i.ip)
                i.delete()
                _l.CPEUTP.ADD(by_representation=user, id_value=i.id,
                        description=f'CPE borrado al actualizar lista CPEUTP de Mikrotik', 
                        entity_representation=repr(i))
                if i.status==True and obj.cpeutp_active != 0:
                    obj.cpeutp_active = obj.cpeutp_active - 1
                if obj.cpeutps != 0:
                    obj.cpeutps = obj.cpeutps - 1
                obj.save()
                _l.MIKROTIK.ADD(by_representation=user, id_value=obj.id,
                    description=f'Eliminado CPE al actualizar lista CPEUTP del nodo',
                    entity_representation=repr(obj))
                    
        else: # All other have already been updated, do nothing
            print("All have been updated")
            pass
        _l.MIKROTIK.ADD(by_representation=user, id_value=elem.uuid,
                description=f'Actualizada lista CPEUTP de Mikrotik', entity_representation=repr(elem))

    except Exception as e:
        _l.MIKROTIK.ADD(by_representation=user, id_value=utp_uuid, id_field='uuid',
        description=f'fallo actualizar lista cpeutp: {e}')

        raise serializers.ValidationError({
          "error" : f'fallo actualizar lista cpeutp: {e}',
          "detail" : str(e)
        })


class OperatorSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Operator
        fields = ('id', 'name', 'code')


class ProfileUTPSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    def create(self, validated_data):
        user = self.context['request'].user
        profile_name = validated_data.pop('name')
        limitation_name = validated_data.pop('limitation_name')
        download_speed = validated_data.pop('download_speed')
        upload_speed = validated_data.pop('upload_speed')
        radius = validated_data.pop('radius')
        
        try:
            radius_conn = RadiusConnection.objects.get(radius__id=radius.id)
            data = {
                    "profile" : profile_name,
                    "limitation" : limitation_name,
                    "download_speed" : download_speed,
                    "upload_speed" : upload_speed
            }
            info = create_radius_profile(radius_conn.radius.ip,radius_conn.username,
                                            radius_conn.password,data)
            if not isinstance(info,dict):
                msg = 'Falló la conexión vía API. Por favor revise que el username soporte API, \
                        y que el IP Services permita la conexión desde las IP 190.113.247.215 y \
                        190.113.247.219.'
                if info!="":
                    msg=msg+"\n"+info
                raise Exception('{}'.format(msg))

            obj = ProfileUTP.objects.create(name=profile_name,limitation_name=limitation_name,
                                            download_speed=download_speed,upload_speed=upload_speed,
                                            radius=radius)
            obj.save()

            _l.PROFILEUTP.ADD(by_representation=user, id_value=obj.id,
                description=f'Nuevo Profile', entity_representation=repr(obj))

            return obj

        except Exception as e:
            _l.PROFILEUTP.ADD(by_representation=user, id_value=radius, id_field='id',
            description=f'fallo crear profile: {e}')

            raise serializers.ValidationError({
              "error" : f'fallo crear profile: {e}',
              "detail" : str(e)
            })

    class Meta:
        model = ProfileUTP
        fields = ('id', 'name','download_speed','upload_speed','provisioning_available',
                    'limitation_name','description','radius','system_profile')

class SystemProfileSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = SystemProfile
        fields = ('id', 'name','download_speed','upload_speed','provisioning_available',
                    'limitation_name','description')


def update_profileutp(profile,i):
    """
    This method supports the main for updating cpe.
    Simply grabs a dict and updates de writable fields of a ProfileUTP instance.
    """
    boolean = True
    if i['name']!=profile.name:
        boolean = False
        profile.name=i['name']
    if i['limitation_name']!=profile.limitation_name:
        boolean = False
        profile.limitation_name=i['limitation_name']
    if "download_speed" in i.keys() and round(int(i['download_speed'])/1000000,0)!=profile.download_speed:
        boolean = False
        profile.download_speed=round(int(i['download_speed'])/1000000,0)
    if "upload_speed" in i.keys() and round(int(i['upload_speed'])/1000000,0)!=profile.upload_speed:
        boolean = False
        profile.upload_speed=round(int(i['upload_speed'])/1000000,0)
    if not boolean:
        profile.save()
        _l.PROFILEUTP.ADD(by_representation=user, id_value=profile.id,
                description=f'Actualizado profile al actualizar lista Profile de RADIUS',
                entity_representation=repr(profile))



def main_updateprofileutp(id_given):
    """
    This is a method used in order to update the profile list of a
    given RADIUS with the information in the device
    """
    # Get RADIUS
    elem = Radius.objects.get(uuid=id_given)
    # Get connections details for this device
    connection_data = RadiusConnection.objects.get(radius=elem)
    user = connection_data.username
    _l.RADIUS.ADD(by_representation=user, id_value=elem.id,
                description=f'Petición para actualizar lista Profile de RADIUS',
                entity_representation=repr(elem))
    try:
        # Get device profiles
        profiles = get_radius_profile(elem.ip,connection_data.username,connection_data.password)
        if not isinstance(profiles,list):
            raise Exception("Error: "+profiles)
        # Get cpes currently associated to utp node
        saved_profile = list(ProfileUTP.objects.filter(radius=elem))
        # Compare the two lists
        temp = []
        while len(profiles)!=0 and len(saved_profile)!=0:
            current = profiles.pop()
            temp.append(current)
            print("This is current element from profile list in device")
            print(current)
            for i in range(len(saved_profile)):
                if (current['name'] == saved_profile[i].name and 
                    current['limitation_name']==saved_profile[i].limitation_name):
                    print("found match in saved profile,updating")
                    update_profileutp(saved_profile[i],current)
                    saved_profile = extract_elem_from_list(saved_profile,i)
                    print("This is the new list")
                    print(saved_profile)
                    temp.pop()
                    break
        if len(profiles)==0:
            profiles = temp
        print(profiles)
        if len(profiles)!= 0 and len(saved_profile)==0: # There is at least one that needs to be added
            print("Need to add new profile(s) to list")
            radius = elem
            for i in profiles:
                print(i)
                try:       
                    if "download_speed" in i.keys():
                        ds=round(int(i['download_speed'])/1000000,0)
                    else:
                        ds=None
                    if "upload_speed" in i.keys():
                        up=round(int(i['upload_speed'])/1000000,0)
                    else:
                        up=None
                    obj = ProfileUTP.objects.create(name=i['name'],
                                                    limitation_name=i['limitation_name'],
                                                    download_speed=ds,
                                                    upload_speed=up,
                                                    radius=radius)
                    obj.save()

                    _l.PROFILEUTP.ADD(by_representation=user, id_value=obj.id,
                        description=f'Nuevo Profile', entity_representation=repr(obj))

                except Exception as e:
                    _l.PROFILEUTP.ADD(by_representation=user, id_value=id_given, id_field='uuid',
                    description=f'fallo crear profile: {e}')

                    raise serializers.ValidationError({
                      "error" : f'fallo crear profile: {e}',
                      "detail" : str(e)
                    })
        elif len(profiles)== 0 and len(saved_profile)!=0: # There is a cpe that needs to be deleted
            print("We need to delete profile(s)")
            for i in saved_profile:
                print("Deleting this element")
                print(i)
                print(i.name)
                i.delete()
                _l.PROFILEUTP.ADD(by_representation=user, id_value=i.id,
                description=f'Borrado profile al actualizar lista Profile de RADIUS',
                entity_representation=repr(i))
                    
        else: # All other have already been updated, do nothing
            print("All have been updated")
            pass
        _l.RADIUS.ADD(by_representation=user, id_value=elem.uuid,
            description=f'Actualizada lista Profile de RADIUS', entity_representation=repr(elem))

    except Exception as e:
        _l.RADIUS.ADD(by_representation=user, id_value=id_given, id_field='uuid',
        description=f'fallo actualizar lista profile: {e}')

        raise serializers.ValidationError({
          "error" : f'fallo actualizar lista profile: {e}',
          "detail" : str(e)
        })


class TypePLANSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = TypePLAN
        fields = ('id', 'name',)


class PlanSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Plan
        fields = ('id','operator','name','type_plan',
                  'aggregation_rate','profile_name','matrix_plan')   


class ProvisioningSerializer(QueryFieldsMixin, serializers.Serializer):

    mik = serializers.CharField()

    def create(self, validated_data):
      
        return validated_data