from rest_framework import viewsets, mixins
from dashboard.models import LogSentinel
from dashboard.serializers import *

class LogViewSet(viewsets.ModelViewSet):

    queryset = LogSentinel.objects.all()
    serializer_class = LogSerializer