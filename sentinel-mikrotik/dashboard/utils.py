import os
import requests
from django.conf import settings
from ua_parser.user_agent_parser import Parse
from dynamic_preferences.registries import global_preferences_registry
from .models import LogSentinel, Entity, TypeAction
from dashboard.actions import *

global_preferences = global_preferences_registry.manager()


class LOG(object):
    
    model = LogSentinel
    _ent = None
    _method = None

    def __getattr__(self, attr):
        name = attr.upper()
        actions = ['ADD','CHANGE','FIND_AUTOFIND','DELETE',
                  'DEL','ERASE','SUP','COUNTERS_UPDATE','SHELL']
        if name in actions:
            self._method = name
            return self
        self._ent = name
        return self


    def get_action_method(self):
        r = None
        if self._method == 'ADD':
            r = ADD_ACTION
        if self._method == 'CHANGE':
            r = CHANGE_ACTION
        if self._method == 'FIND_AUTOFIND':
            r = FIND_AUTOFIND_ACTION
        if self._method in ['DELETE','DEL','ERASE','SUP']:
            r = DELETE_ACTION
        if self._method == 'COUNTERS_UPDATE':
            r = COUNTERS_UPDATE_ACTION
        if self._method == 'SHELL':
            r = SHELL_ACTION
        if self._method == 'GET':
            r = GET_ACTION

        print ('RRRRRRRRRR',r)
        return r

    def __call__(self, *args, **values):
        

        if len(args) == 1:
            return self.save_args(*args, **values)

        action = self.get_action_method()
        if action:
            return self.save_log(action, **values)

        print ('me estan llamando sin funcion',values,self._method, self._ent)

    def save_args(self, *args, **values):
        data_local = dict(args[0])
        data_send = {}

        action = self.get_action_method()
        if action:

            data_send['id_data']=data_local.get('_id_data', None)
            if 'obj_to_log' in data_local:
                data_send['by_representation'] = data_local['obj_to_log']['by_representation']

            if SHELL_ACTION == action:

                if 'e' in data_local:
                    sms = f'No se pudo conectar -> {data_local["e"]}'
                    data_send['level'] = 30
                else:
                    sms = 'Nueva conexión'

                data_send['description']=sms
                data_send['id_value']=data_local.get('olt').uuid
                data_send['entity_representation']=repr(data_local.get('olt'))
                return self.save_log(SHELL_ACTION,**data_send)

            if ADD_ACTION == action:
                data_send['description']='Añadido con exito'

                if self._ent == 'INTERFACE':
                    data_send['entity_representation']=repr(data_local.get('interface'))
                    data_send['id_value']=data_local.get('interface').id

                return self.save_log(SHELL_ACTION,**data_send)


    def save_log(self, action_method, **values):
        print ('save_log',action_method,values)
        if self._ent != 'OLT':
            #exit()
            pass
        entity, c = Entity.objects.get_or_create(entity=self._ent)
        action, c = TypeAction.objects.get_or_create(type_action = values.get('action', action_method))

        entity_representation = values.get('entity_representation',str(entity))

        by = values.get('by', None)
        by_representation = values.get('by_representation')
        id_field = values.get('id_field', None)
        id_value = values.get('id_value', None)
        description = values.get('description')
        id_data = values.get('id_data', None)
        level = values.get('level',None)

        data = {
            'by_representation':by_representation,
            'action':action,
            'entity_representation':entity_representation,
            'entity':entity,
            'description':description,
            'id_data':id_data
        }

        if id_value:
            data['id_value'] = id_value

        if id_field:
            data['id_field'] = id_field

        if level:
            data['level'] = level

        if by:
            data['by'] = by


        self.model.objects.create(**data)
        return True