from rest_framework.serializers import Serializer
from dashboard.models import LogSentinel
    

__all__ = ['LogSerializer']


class LogSerializer(Serializer):

    class Meta:
        model = LogSentinel
        fields = ('id', 'action','by','by_representation','entity',
                    'entity_representation','id_field','id_value',
                    'description','level')